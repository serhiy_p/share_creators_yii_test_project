<?php
/**
 * Created at 3/29/18 6:39 PM by Seven
 * Copyright (c) 2018, Share Creators. All rights reserved.
 */

namespace app\models;

use yii\db\ActiveRecord;

class Artist extends ActiveRecord
{

    public static function tableName()
    {
        return 'artists';
    }

    public function rules()
    {
        return [
            [['name', 'email', 'city'], 'required'],
            ['email', 'email'],
            ['email', 'unique'],
            ['name', 'string', 'max'=>'50'],
            ['city', 'string', 'max'=>'30'],
            ['email', 'string', 'max'=>'30'],
        ];
    }
}