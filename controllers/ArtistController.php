<?php
/**
 * Created at 3/29/18 6:39 PM by Seven
 * Copyright (c) 2018, Share Creators. All rights reserved.
 */

namespace app\controllers;

use yii\rest\ActiveController;

class ArtistController extends ActiveController
{
    public $modelClass = 'app\models\Artist';
}